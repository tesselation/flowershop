# frozen_string_literal: true

RSpec.describe FlowerShop::Catalogue do
  describe '#initialize' do
    subject(:catalogue) { described_class.new(flower_code) }

    let(:flower_code) { 'R12' }

    let(:fixture_data) do
      {
        flowers: [
          {
            code: flower_code,
            bunches: [
              {
                size: 5,
                price: '6.99'
              },
              {
                size: 10,
                price: '12.99'
              }
            ]
          },
          code: 'foo',
          bunches: [
            {
              size: 1,
              price: 'a price'
            }
          ]
        ]
      }
    end

    before do
      allow(JSON).to receive(:parse).and_return(fixture_data)
    end

    it 'loads the correct flower code' do
      expect(catalogue.flower_code).to eq(flower_code)
    end

    it 'loads the relevant bunch data' do
      expect(catalogue.bunches.length).to eq(2)
    end
  end
end
