# frozen_string_literal: true

RSpec.describe FlowerShop::PriceFormatter do
  describe '.call' do
    subject(:formatted_price) { described_class.call(order_result) }

    let(:order_result) do
      [{ bunch_size: 5, price: '9.95', quotient: 2, remainder: 3 },
       { bunch_size: 3, price: '5.95', quotient: 1, remainder: 0 }]
    end

    it 'formats the price into a string' do
      expect(formatted_price).to eq('$25.85 - 2 x 5 $9.95, 1 x 3 $5.95')
    end

    context 'when to total ends in 0' do
      let(:order_result) do
        [{ bunch_size: 5, price: price, quotient: 1, remainder: 0 }]
      end
      let(:price) { '9.90' }

      it 'includes the 0 in the calculated price' do
        expect(formatted_price[1..4]).to eq(price)
      end
    end

    context 'when the order_result includes orders of zero' do
      let(:order_result) do
        [{ bunch_size: 5, price: '9.95', quotient: 2, remainder: 3 },
         { bunch_size: 4, price: '8.95', quotient: 0, remainder: 3 },
         { bunch_size: 3, price: '5.95', quotient: 1, remainder: 0 }]
      end

      let(:expected_result) do
        '$25.85 - 2 x 5 $9.95, 1 x 3 $5.95'
      end

      it 'doesn\'t include them' do
        expect(formatted_price).to eq(expected_result)
      end
    end
  end
end
