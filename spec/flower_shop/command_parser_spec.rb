# frozen_string_literal: true

RSpec.describe FlowerShop::CommandParser do
  subject(:parsed_input) { described_class.parse(input) }

  context 'with valid input' do
    let(:input) { "#{num} #{code}" }

    let(:num) { 10 }
    let(:code) { 'R12' }

    it 'coerces the number to an integer' do
      expect(parsed_input[:number]).to be_kind_of(Integer)
    end

    it 'returns the correct value for the number' do
      expect(parsed_input[:number]).to eq(num)
    end

    it 'returns the correct value for the flower code' do
      expect(parsed_input[:flower_code]).to eq(code)
    end
  end

  context 'with invalid input' do
    context 'with input that does not have an integer' do
      let(:input) { 'one R12' }

      it 'raises a Dry::Types::CoercionError error' do
        expect { parsed_input }.to raise_error Dry::Types::CoercionError
      end
    end

    context 'with an invalid flower code' do
      let(:input) { '10 R10' }

      it 'raises a Dry::Types::ConstraintError error' do
        expect { parsed_input }.to raise_error Dry::Types::ConstraintError
      end
    end
  end
end
