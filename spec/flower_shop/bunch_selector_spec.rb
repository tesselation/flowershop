# frozen_string_literal: true

RSpec.describe FlowerShop::BunchSelector do
  let(:parsed_order) { { number: number, flower_code: flower_code } }
  let(:flower_code) { 'R12' }
  let(:number) { 10 }

  describe '#initialize' do
    subject(:selector) { described_class.new(parsed_order) }

    before do
      catalogue = class_double(FlowerShop::Catalogue).as_stubbed_const
      allow(catalogue).to receive(:new)
      selector
    end

    it 'creates a catalogue for the selected flower' do
      expect(FlowerShop::Catalogue).to have_received(:new).with(flower_code)
    end
  end

  describe '#select_flowers' do
    subject(:selected_flowers) do
      described_class.new(parsed_order).select_flowers
    end

    before do
      catalogue = instance_double(
        FlowerShop::Catalogue,
        bunches: bunches,
        flower_code: flower_code
      )
      allow(FlowerShop::Catalogue).to receive(:new).and_return(catalogue)
    end

    let(:bunches) do
      [{ size: 5, price: '6.99' }, { size: 10, price: '12.99' }]
    end

    context 'when order can be fulfilled' do
      let(:expected_result) do
        [{ bunch_size: 10, price: '12.99', quotient: 1, remainder: 0 },
         { bunch_size: 5, price: '6.99', quotient: 0, remainder: 0 }]
      end

      it 'gives information about the optimal bunch makeup' do
        expect(selected_flowers).to eq(expected_result)
      end
    end

    context 'when a bundle cannot be made using the largest bundle' do
      let(:flower_code) { 'T58' }
      let(:number) { 13 }

      let(:bunches) do
        [{ size: 3, price: '5.95' },
         { size: 5, price: '9.95' },
         { size: 9, price: '16.99' }]
      end

      let(:expected_result) do
        [{ bunch_size: 5, price: '9.95', quotient: 2, remainder: 3 },
         { bunch_size: 3, price: '5.95', quotient: 1, remainder: 0 }]
      end

      it 'gives information about the optimal bunch makeup' do
        expect(selected_flowers).to eq(expected_result)
      end
    end

    context 'when a bundle cannot be made up' do
      let(:flower_code) { 'R12' }
      let(:number) { 9 }

      it 'raises an error' do
        expect { selected_flowers }.to raise_error(StandardError)
      end
    end
  end
end
