# frozen_string_literal: true

RSpec.describe FlowerShop do
  describe 'processing an order given input' do
    subject(:result) { described_class.process_order(order) }

    context 'with valid input' do
      context 'when one bundle is required' do
        let(:order) { '10 R12' }

        it 'outputs the price and bundle makeup' do
          expect { result }.to output("$12.99 - 1 x 10 $12.99\n").to_stdout
        end
      end

      context 'when more than one bundle is required' do
        let(:order) { '15 L09' }

        it 'outputs the price and bundle makeup' do
          expect { result }.to output(
            "$41.90 - 1 x 9 $24.95, 1 x 6 $16.95\n"
          ).to_stdout
        end
      end

      context 'when a bundle cannot be made using the largest bundle' do
        let(:order) { '13 T58' }

        it 'outputs the price and bundle makeup' do
          expect { result }.to output(
            "$25.85 - 2 x 5 $9.95, 1 x 3 $5.95\n"
          ).to_stdout
        end
      end

      context 'when the order cannot be fulfilled' do
        let(:order) { '5 L09' }

        it 'outputs a friendly message' do
          expect { result }.to output(
            "Apologies, cannot fulfil #{order} with given bundles\n"
          ).to_stdout
        end
      end
    end

    context 'with invalid input' do
      let(:order) { 'a meaningless string' }

      it 'outputs a friendly message' do
        expect { result }.to output(
          "Apologies, I do not understand\n"
        ).to_stdout
      end
    end
  end
end
