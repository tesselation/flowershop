# frozen_string_literal: true

require 'simplecov'
SimpleCov.start

Dir[Dir.pwd + '/lib/**/*.rb'].each { |file| require file }

RSpec.configure do |c|
  c.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  c.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  c.shared_context_metadata_behavior = :apply_to_host_groups
  c.filter_run_when_matching :focus
  c.disable_monkey_patching!
  c.order = :random
  c.default_formatter = 'doc' if c.files_to_run.one?

  Kernel.srand c.seed
end
