# frozen_string_literal: true

begin
  require 'rubocop/rake_task'

  RuboCop::RakeTask.new(:rubocop) do |task|
    task.patterns = %w[
      lib/**/*.rb
      spec/**/*.rb
    ]
  end
rescue LoadError
  warn 'rubocop not available, rake task not provided.'
end
