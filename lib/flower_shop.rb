# frozen_string_literal: true

require_relative 'flower_shop/bunch_selector'
require_relative 'flower_shop/command_parser'
require_relative 'flower_shop/price_formatter'

module FlowerShop
  class << self
    def process_order(args)
      order = CommandParser.parse(args)
      bunch_selector = BunchSelector.new(order)
      puts PriceFormatter.call(bunch_selector.select_flowers)
    rescue Dry::Types::CoercionError, Dry::Types::ConstraintError
      puts 'Apologies, I do not understand'
    rescue StandardError
      puts "Apologies, cannot fulfil #{args} with given bundles"
    end
  end
end
