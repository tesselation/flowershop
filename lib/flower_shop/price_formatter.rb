# frozen_string_literal: true

module FlowerShop
  class PriceFormatter
    class << self
      def call(order_result)
        "$#{total(order_result)} - #{order_makeup(order_result)}"
      end

      private

      def total(order_result)
        decimal_price = order_result.reduce(0) do |sum, x|
          sum + BigDecimal(x[:price]) * Integer(x[:quotient])
        end
        string_price_no_rounding(decimal_price)
      end

      def order_makeup(result)
        result.collect do |bunch|
          unless bunch[:quotient].zero?
            "#{bunch[:quotient]} x #{bunch[:bunch_size]} $#{bunch[:price]}"
          end
        end.compact.join(', ')
      end

      def string_price_no_rounding(decimal)
        format('%.2f', BigDecimal(decimal).truncate(2))
      end
    end
  end
end
