# frozen_string_literal: true

require_relative './catalogue'

module FlowerShop
  class BunchSelector
    def initialize(number:, flower_code:)
      @catalogue = FlowerShop::Catalogue.new(flower_code)
      @number = number
    end

    def select_flowers
      result = try_each_subset_of_bunches
      raise StandardError unless successful?(result)

      result
    end

    private

    def try_each_subset_of_bunches
      sorted_all_subsets_array.each do |set|
        result = iterate_flowers(set, @number)
        break result if successful?(result)
      end
    end

    def iterate_flowers(bunches_array, flowers_required)
      bunches_array.map do |bunch_size|
        quotient, remainder = flowers_required.divmod(bunch_size)
        flowers_required = remainder
        { bunch_size: bunch_size,
          quotient: quotient,
          remainder: remainder,
          price: price_of_selected(bunch_size) }
      end
    end

    def price_of_selected(size)
      @catalogue.bunches.find do |bunch|
        bunch[:size] == size
      end[:price]
    end

    def successful?(bunch_results)
      bunch_results.any? { |x| x[:remainder].zero? }
    end

    def sorted_all_subsets_array
      all_subsets_array.sort { |x, y| y.length <=> x.length }
    end

    def all_subsets_array
      1.upto(sorted_bunches_array.length).flat_map do |n|
        sorted_bunches_array.combination(n).to_a
      end
    end

    def sorted_bunches_array
      @catalogue.bunches.map { |bunch| bunch[:size] }
                .sort { |x, y| y <=> x }
    end
  end
end
