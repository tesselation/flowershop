# frozen_string_literal: true

require 'json'

module FlowerShop
  class Catalogue
    attr_reader :flower_code, :bunches

    def initialize(flower_code)
      @flower_code = flower_code
      @bunches = data[:bunches]
    end

    private

    def data
      catalogue_data.dig(:flowers).find do |flower|
        flower[:code] == flower_code
      end
    end

    def catalogue_data
      data_file = File.expand_path('../../catalogue-data.json', __dir__)
      JSON.parse(File.read(data_file), symbolize_names: true)
    end
  end
end
