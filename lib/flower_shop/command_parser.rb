# frozen_string_literal: true

require 'dry-types'

module Types
  include Dry::Types()
end

module FlowerShop
  class CommandParser
    class << self
      def parse(command)
        number, flower_code = command.split(' ')
        {
          number: Types::Coercible::Integer[number],
          flower_code: valid_flower_code(flower_code)
        }
      end

      private

      FLOWER_CODES = %w[R12 L09 T58].freeze

      def valid_flower_code(code)
        Types::Strict::String.constrained(included_in: FLOWER_CODES)[code]
      end
    end
  end
end
