# frozen_string_literal: true

require './lib/flower_shop'

if $PROGRAM_NAME == __FILE__
  $stdout.sync = true
  FlowerShop.process_order(ARGV.join(' '))
end
