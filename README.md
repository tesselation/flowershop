# Flower Shop Test

## Description

This is an implementation of the Flower Shop coding test as described in [the brief](./flower_shop.pdf).
This is a command line app, when given a number of flowers in the format 'n flowercode' will return the minimum number of bundles required to fulfil that order - from the bundles described in the brief.

## Environments

This was developed using `Docker for Mac version 19.03.1, build 74b1e89`

The ruby image used is `ruby:2.6-alpine` and was developed on `sha256:a546d6c5f530bbcc2d41fbf0da21d94da9f70cba2997c82b57b06ea6a2ef155d`
The target ruby version this was developed on is `ruby 2.6`. While it should work on any ruby version greater than 2, it has not been tested on any other version

## System Dependencies & Configuration

### Docker

This can be run with docker and docker-compose installed and with internet connectivity in order to pull the base image from docker hub

### Running locally

#### Prerequisites

- ruby 2.6
- xcode developer tools
- bundler gem (`gem install bundler`)

#### Dependencies

In order to install the dependencies navigate to the root of this project and run `bundle install`

## Usage instructions

### Docker

In order to run the test suite and rubocop

`docker-compose run test`

In order to run the application

`docker-compose run base ruby flower_shop_app.rb $flowernumber $flowercode` where $flowernumber and $flowercode are the number of flowers and the code for the flower type that you wish to purchase respectively

### Locally

In order to run the test suite and rubocop, after installing the dependencies rub

`bundle exec rake`

In order to run the application

`ruby flower_shop_app.rb $flowernumber $flowercode` where $flowernumber and $flowercode are the number of flowers and the code for the flower type that you wish to purchase respectively

## Overview

This application is designed to read a number and a flowercode provided as arguments to a ruby script. It will return the bundle makeup as a string to `$stdout`

### Input format

This expects input such as `10 R12` where `10` is the number of flowers and `R12` is the flower code.
Allowed flower codes from the brief are R12, L09, T58.

### Output format

The program will output to stdout, a string with the format

`$total - $number x $bundle_size $bundle_price, $number x $bundle_size $bundle_price`


## Design

### Launch

`flower_shop_app` is a ruby script which executes the main application and sets STDOUT sync mode to true so that output is immediately printed

### lib/flower_shop.rb

This is a controller-like class. It passes the input to a parser class, and then passes that input to a class the selects the minimum number of bundles to fulfil the order. Finally, it passes to a price formatter class, which both calculates the total price and formats it to the output required

### lib/command_parser.rb

This class takes the input, validates it and coerces it into an Integer and a recognised flower code.

### lib/bunch_selector.rb

This is where the bulk of the logic lives.
It creates a new instance of the catalogue class for the specific flower and which contains the bundle sizing and pricing information.

I considered a few approaches to the problem, and in the end - given that at most there are three different bunch sizes, I decided on a brute force approach which tries to see if the order can be fulfilled using every combination of bunch size.

This is obviously computationally expensive. If this program were to scale, or if the number of bundle sizes were to grow this approach would need to be reconsidered. Is it imperative that the fewest bundles are always chosen? Or could another knapsack problem algorithm solution work?

### lib/catalogue.rb

This is a simple class which reads the flower data from a json file and returns information about how the specified flower can be brought

### lib/price_formatter.rb

This class takes the results of the bunch_selector, and calculates the total price and formats a response with the information about the price and the make-up of that price

## Discussion

Improvements

- I have just noticed while writing the readme that the response should also include the flower code. While this would be easy enough to add, It's getting late and I'm calling the code test done
- The PriceFormatter is doing both calculation and formatting work, which is too much. Especially given that I've just realised that there's an additional piece of information to be formatted in. I'd like to split it out into a TotalPrice and Formatter class
- ~~The bundle_selector tests don't mock the catalogue class, and as a result are using the actual catalogue-data.json data. This means that if the base data changes (eg. the prices or flower bundles configurations) the tests will break. I wanted to create a proper test fixture but ran out of time.~~
- I sank a bit of time into reading about algorithmic approaches to the knapsack problem, but in the end decided on an approach that that tried every subset of flower bundles. Technically an approach where the head of the list was removed and then the list was tried again would work for all provided inputs, I decided on an approach that would work in more situations. The wikipedia article lists other algorithmic approaches - I would need to take time to properly understand them
- Currently the bunch_selector class returns an Array of Hashes, and there is a `successful?()` method in the class to determine if the order has been fulfiled. I would like to create a Result class that contains the data to be returned instead, which could then respond to `successful?` instead. I think it would tidy up the class a little.
- Some of the Docker scripts could be tidied up a little bit to make the application easier to run
- The flower codes used in the validation part of the parser are hard coded and don't come from the catalogue-data.json. I would have liked to make sure that there was a single source of truth for valid flower_codes
