FROM ruby:2.6-alpine

ENV APP_HOME /app
WORKDIR $APP_HOME

ADD Gemfile $APP_HOME/Gemfile
ADD Gemfile.lock $APP_HOME/Gemfile.lock

RUN apk --no-cache --virtual .build-deps add gcc make g++ && \
    bundle install --jobs 8 --retry 3 && \
    apk del .build-deps

COPY . $APP_HOME
